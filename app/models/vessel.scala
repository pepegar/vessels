package models

case class Vessel(name: String,
                  width: Double,
                  length: Double,
                  draft: Double,
                  coordinates: Coordinates)

case class Coordinates(latitude: Double,
                       longitude: Double)

object JsonFormats {
  import play.api.libs.json.Json

  implicit val coordinateFormat = Json.format[Coordinates]
  // Generates Writes and Reads for Feed and Vessel thanks to Json Macros
  implicit val vesselFormat = Json.format[Vessel]
}
