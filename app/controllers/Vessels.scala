package controllers

import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import reactivemongo.bson.BSONDocument
import reactivemongo.core.commands.LastError
import scala.concurrent.Future
import reactivemongo.api.Cursor
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import org.slf4j.{LoggerFactory, Logger}
import javax.inject.Singleton
import play.api.mvc._
import play.api.libs.json._

import scala.util.{Success, Failure}
import scala.util.parsing.json.JSONObject

@Singleton
class Vessels extends Controller with MongoController {

  private final val logger: Logger = LoggerFactory.getLogger(classOf[Vessels])

  def collection: JSONCollection = db.collection[JSONCollection]("vessels")

  import models._
  import models.JsonFormats._

  def createVessel = Action.async(parse.json) {
    request =>
      request.body.validate[Vessel].map {
        vessel =>
          collection.insert(vessel).map {
            lastError =>
              logger.debug(s"Successfully inserted with LastError: $lastError")
              Created(s"Vessel Created")
          }
      }.getOrElse(Future.successful(BadRequest("invalid json")))
  }

  def updateVessel(name: String) = Action.async(parse.json) {
    request =>
      request.body.validate[Vessel].map {
        vessel =>
          val nameSelector = Json.obj("name" -> name)
          collection.update(nameSelector, vessel).map {
            lastError =>
              logger.debug(s"Successfully updated with LastError: $lastError")
              Created(s"Vessel Updated")
          }
      }.getOrElse(Future.successful(BadRequest("invalid json")))
  }

  def findVessels = Action.async {
    val cursor: Cursor[Vessel] = collection
      .find(Json.obj())
      .cursor[Vessel]

    val futureVesselsList: Future[List[Vessel]] = cursor.collect[List]()

    val futureVesselsJsonArray: Future[JsArray] = futureVesselsList.map { vessels =>
      Json.arr(vessels)
    }

    futureVesselsJsonArray.map {
      vessels =>
        Ok(vessels(0))
    }
  }

  def findVessel(name: String) = Action.async {
    import JsonFormats._

    val cursor: Cursor[Vessel] = collection
      .find(Json.obj("name" -> name))
      .cursor[Vessel]


    val futureVessel: Future[Option[Vessel]] = cursor.headOption

    futureVessel.map {
      case None => NotFound("vessel not found")
      case Some(vessel) => Ok(Json.toJson(vessel))
    }
  }

  def deleteVessel(name: String) = Action.async {
    val result = collection
      .remove(Json.obj("name" -> name))

    result.map {
      case LastError(true, _, _, _, _, _, _) => Ok("successfully deleted")
      case LastError(false, err, _, _, _, _, _) => BadRequest(err.get)
    }
  }
}
