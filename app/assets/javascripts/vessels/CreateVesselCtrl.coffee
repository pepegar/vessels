class CreateVesselCtrl

    constructor: (@$log, @$location,  @VesselService) ->
        @$log.debug "constructing CreateVesselController"
        @Vessel = {}

    createVessel: () ->
        @$log.debug "createVessel()"
        @VesselService.createVessel(@Vessel)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data} Vessel"
                @Vessel = data
                @$location.path("/")
            ,
            (error) =>
                @$log.error "Unable to create Vessel: #{error}"
            )

controllersModule.controller('CreateVesselCtrl', ['$log', '$location', 'VesselService', CreateVesselCtrl])
