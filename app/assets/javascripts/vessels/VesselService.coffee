
class VesselService

    @headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
    @defaultConfig = { headers: @headers }

    constructor: (@$log, @$http, @$q) ->
        @$log.debug "constructing VesselService"

    listVessels: () ->
        @$log.debug "listVessels()"
        deferred = @$q.defer()

        @$http.get("/vessels")
        .success((data, status, headers) =>
                @$log.info("Successfully listed vessels - status #{status}")
                deferred.resolve(data)
            )
        .error((data, status, headers) =>
                @$log.error("Failed to list vessels - status #{status}")
                deferred.reject(data)
            )
        deferred.promise

    createVessel: (Vessel) ->
        @$log.debug "createVessel #{angular.toJson(Vessel, true)}"
        deferred = @$q.defer()

        @$http.post('/vessels', Vessel)
        .success((data, status, headers) =>
                @$log.info("Successfully created Vessel - status #{status}")
                deferred.resolve(data)
            )
        .error((data, status, headers) =>
                @$log.error("Failed to create Vessel - status #{status}")
                deferred.reject(data)
            )
        deferred.promise

    updateVessel: (name, Vessel) ->
      @$log.debug "updateVessel #{angular.toJson(Vessel, true)}"
      deferred = @$q.defer()

      @$http.put("/vessels/#{name}", Vessel)
      .success((data, status, headers) =>
              @$log.info("Successfully updated Vessel - status #{status}")
              deferred.resolve(data)
            )
      .error((data, status, header) =>
              @$log.error("Failed to update Vessel - status #{status}")
              deferred.reject(data)
            )
      deferred.promise

    findVessel: (name) ->
      deferred = @$q.defer()

      @$http.get("/vessels/#{name}")
      .success((data, status, headers) =>
              @$log.info("retrieved Vessel #{JSON.stringify(data)}")
              deferred.resolve(data)
            )
      .error((data, status, header) =>
              @$log.error("Vessel not found - status #{status}")
              deferred.reject(data)
            )
      deferred.promise

    deleteVessel: (name) ->
      deferred = @$q.defer()

      @$http.delete("/vessels/#{name}")
      .success((data, status, headers) =>
              @$log.info("retrieved Vessel #{JSON.stringify(data)}")
              deferred.resolve(data)
            )
      .error((data, status, header) =>
              @$log.error("Vessel not found - status #{status}")
              deferred.reject(data)
            )
      deferred.promise

servicesModule.service('VesselService', ['$log', '$http', '$q', VesselService])
