
class VesselCtrl

    constructor: (@$log, @VesselService) ->
        @$log.debug "constructing VesselController"
        @Vessels = []
        @getAllVessels()

    getAllVessels: () ->
        @$log.debug "getAllVessels()"

        @VesselService.listVessels()
        .then(
            (data) =>
                @$log.debug "Promise returned #{data.length} Vessels"
                @Vessels = data
            ,
            (error) =>
                @$log.error "Unable to get Vessels: #{error}"
            )
            
    deleteVessel: (name) ->
        @$log.debug "deleteVessel()"

        @VesselService.deleteVessel(name)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data.length} Vessels"
                @Vessels = data
            ,
            (error) =>
                @$log.error "Unable to get Vessels: #{error}"
            )

controllersModule.controller('VesselCtrl', ['$log', 'VesselService', VesselCtrl])
