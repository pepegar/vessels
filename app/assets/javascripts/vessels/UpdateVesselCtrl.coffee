class UpdateVesselCtrl

  constructor: (@$log, @$location, @$routeParams, @VesselService) ->
      @VesselService.findVessel(@$routeParams.name)
        .then((vessel) => @Vessel = vessel)
      @$log.debug @Vessel

  updateVessel: () ->
      @$log.debug "updateVessel()"
      @VesselService.updateVessel(@$routeParams.name, @Vessel)
      .then(
          (data) =>
            @$log.debug "Promise returned #{data}"
            @Vessel = data
            @$location.path("/")
        ,
        (error) =>
            @$log.error "Unable to update Vessel: #{error}"
      )

controllersModule.controller('UpdateVesselCtrl', ['$log', '$location', '$routeParams', 'VesselService', UpdateVesselCtrl])
