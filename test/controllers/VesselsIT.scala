package controllers

import java.util.concurrent.TimeUnit

import models.Vessel
import models.JsonFormats._
import org.specs2.mutable._
import play.api.libs.json._
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent._
import scala.concurrent.duration._


/**
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class VesselsIT extends Specification {

  val timeout: FiniteDuration = FiniteDuration(5, TimeUnit.SECONDS)

  "Vessels" should {

    "retrieve all the vessels" in {
      running(FakeApplication()) {
        route(FakeRequest.apply(POST, "/vessels").withJsonBody(Json.obj(
          "name" -> "vessel1",
          "width" -> 234.3,
          "length" -> 432.3,
          "draft" -> 432.3,
          "coordinates" -> Json.obj(
            "latitude" -> 0.00000,
            "longitude" -> 0.00000
          ))))
        val request = FakeRequest.apply(GET, "/vessels")
        val response = route(request)
        response.isDefined mustEqual true
        val jsonString = contentAsString(response.get)

        val vessels = Json.parse(jsonString).as[Seq[Vessel]]

        vessels.length must beGreaterThan(0)
      }
    }

    "retrieve a single vessel" in {
      running(FakeApplication()) {
        route(FakeRequest.apply(POST, "/vessels").withJsonBody(Json.obj(
          "name" -> "vesselToRetrieve",
          "width" -> 234.3,
          "length" -> 432.3,
          "draft" -> 432.3,
          "coordinates" -> Json.obj(
            "latitude" -> 0.00000,
            "longitude" -> 0.00000
          ))))
        val request = FakeRequest.apply(GET, "/vessels/vesselToRetrieve")
        val response = route(request)
        response.isDefined mustEqual true
        val jsonString = contentAsString(response.get)

        val vessel = Json.parse(jsonString).as[Vessel]

        vessel.name must equalTo("vesselToRetrieve")
      }
    }

    "insert a valid json" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(POST, "/vessels").withJsonBody(Json.obj(
          "name" -> "vessel1",
          "width" -> 234.3,
          "length" -> 432.3,
          "draft" -> 432.3,
          "coordinates" -> Json.obj(
            "latitude" -> 0.00000,
            "longitude" -> 0.00000
          )))
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        result.header.status must equalTo(CREATED)
      }
    }

    "fail inserting a non valid json" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(POST, "/vessels").withJsonBody(Json.obj(
          "firstName" -> 98,
          "lastName" -> "London",
          "age" -> 27))
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        contentAsString(response.get) mustEqual "invalid json"
        result.header.status mustEqual BAD_REQUEST
      }
    }

    "update a valid json" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(PUT, "/vessels/vessel1").withJsonBody(Json.obj(
          "name" -> "vessel1",
          "width" -> 1.3,
          "length" -> 5.3,
          "draft" -> 432.3,
          "coordinates" -> Json.obj(
            "latitude" -> 1234.00000,
            "longitude" -> 4321.00000
          )))
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        result.header.status must equalTo(CREATED)
      }
    }

    "fail updating a non valid json" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(PUT, "/vessels/vessel1").withJsonBody(Json.obj(
          "name" -> "vessel1",
          "draft" -> 432.3,
          "coordinates" -> Json.obj(
            "latitude" -> 1234.00000,
            "longitude" -> 4321.00000
          )))
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        contentAsString(response.get) mustEqual "invalid json"
        result.header.status mustEqual BAD_REQUEST
      }
    }

    "delete a vessel" in {
      running(FakeApplication()) {
        route(FakeRequest.apply(POST, "/vessels").withJsonBody(Json.obj(
          "name" -> "vesselToDelete",
          "width" -> 234.3,
          "length" -> 432.3,
          "draft" -> 432.3,
          "coordinates" -> Json.obj(
            "latitude" -> 0.00000,
            "longitude" -> 0.00000
          ))))
        val request = FakeRequest.apply(DELETE, "/vessels/vesselToDelete")
        val response = route(request)
        val result = Await.result(response.get, timeout)
        contentAsString(response.get) mustEqual "successfully deleted"
        result.header.status mustEqual OK
      }
    }
  }
}
