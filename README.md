Vessel management application
=============================

_by José Luis García <jl.garhdez@gmail.com>_

## project structure
the project structure was generated using the [modern-web-template](https://github.com/lashford/modern-web-template) activator template:
```
activator new vessels modern-web-template
```

## running and testing
for managing the application you can use the `sbt` script in the root of the project:

```
sbt run # for running
```

```
sbt test # for testing
```

## dependencies
You need MongoDB running in order to use the application.  Once MongoDB is
running, please modify the configuration in `conf/application.conf` and add the
full MongoDB configuration uri for the key `mongodb.uri`.
